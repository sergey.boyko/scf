/**
* Created by Sergey O. Boyko on 27.03.19.
*/

#ifndef SELF_CHECK_FORMAT_DETAIL_SPECIFIER_INDEX_H
#define SELF_CHECK_FORMAT_DETAIL_SPECIFIER_INDEX_H

#include <type_traits>

namespace scf::detail {

/**
* The structure that contains the information about specifier:
* @tparam Specifier - specifier identifier
* @tparam Index - index of the specifier
*/
template<char Specifier, std::size_t Index>
struct SpecifierIndex {
  constexpr static auto specifier = Specifier;
  constexpr static auto index = Index;
};

} // end of scf::detail

#endif //SELF_CHECK_FORMAT_DETAIL_SPECIFIER_INDEX_H
