/**
* Created by Sergey O. Boyko on 20.04.19.
*
* The header contains the function that convert arguments to string using specifiers
*/

#ifndef SELF_CHECK_FORMAT_DETAIL_TO_STRING_H
#define SELF_CHECK_FORMAT_DETAIL_TO_STRING_H

#include <string>

#include <scf/detail/specifiers.h>

namespace scf::detail {

template<
    char Specifier,
    typename T,
    std::enable_if_t<Specifier == specifiers::string_spc_k> * = nullptr
>
std::string ToString(const T &arg) {
  return {arg};
}

template<
    char Specifier,
    std::enable_if_t<Specifier == specifiers::string_spc_k> * = nullptr
>
std::string ToString(std::string_view arg) {
  return arg.data();
}

template<
    char Specifier,
    std::enable_if_t<Specifier == specifiers::char_spc_k> * = nullptr
>
std::string ToString(char arg) {
  return {arg};
}

template<
    char Specifier,
    typename T,
    std::enable_if_t<
        Specifier == specifiers::int_spc_k
        || Specifier == specifiers::float_spc_k> * = nullptr
>
std::string ToString(T arg) {
  return std::to_string(arg);
}

template<
    char Specifier,
    std::enable_if_t<Specifier == specifiers::bool_spc_k> * = nullptr
>
std::string ToString(bool arg) {
  return arg ? "true" : "false";
}

template<
    char Specifier,
    typename T,
    std::enable_if_t<Specifier == specifiers::user_type_spc_k> * = nullptr
>
std::string ToString(const T &arg) {
    return ToString(arg);
}

} // end of scf::detail

#endif //SELF_CHECK_FORMAT_DETAIL_TO_STRING_H
