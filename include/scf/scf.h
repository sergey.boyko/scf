/**
* Created by Sergey O. Boyko on 24.03.19.
*/

#ifndef SELF_CHECK_FORMAT_SCF_H
#define SELF_CHECK_FORMAT_SCF_H

#include <cstring>

#include <scf/detail/format_impl.h>

/**
* The macro that wraps the str in a lambda function
* to process the str as constexpr value
*/
#define SCFormat(str, ...) \
::scf::detail::FormatImpl([](){return str;}, ##__VA_ARGS__)

#endif //SELF_CHECK_FORMAT_SCF_H
