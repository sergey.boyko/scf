# Self-check-logger
### Compile time string formatting using template metaprogramming

Self-check-logger (abbreviated as scf) is a small library that provides the ability to:
- check if the input arguments match the format specifiers and if it isn’t, the compiler is required to issue an error message and stop the compiling process
- calculate indexes of the specifiers at compile time to unload runtime formatting (available non-constexpr arguments)
- insert the arguments in the resulting string replacing their respective specifiers at runtime

### Motivating example
```c++
std::string arg1 = "world";
std::size_t arg2 = 17;
bool arg3 = true;
std::cout << SCFormat("Hello, %s. Required C++ standard - %d, it's %b",
                         arg1, arg2, arg3);

// compile error
// std::cout << SCFormat("Hello, %d. Required C++ standard - %s, it's %c",
//                          arg1, arg2, arg3);
```
Outputs:
```
Hello, world. Required C++ standard - 17, it's true
```
