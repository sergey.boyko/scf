#!/bin/sh

# Print an error
echoerr() {
  echo "ERROR: $@" 1>&2;
}

# Print the script usage
print_usage() {
  echo ""
  echo "Usage: $0 [OPTION]..."
  echo ""
  echo "options:"
  echo "  -h --help     Show help "
  echo "  --with-tests  Build and run tests"
  echo "  --prefix=DIR  Install self-check-logger and tests"
}

# Print formatted state
print_state() {
  title="| $1 |"
  edge=$(echo "$title" | sed 's/./-/g')
  echo ""
  echo "$edge"
  echo "$title"
  echo "$edge"
  echo ""
}

BUILD_TESTS=OFF

# Process arguments
while [[ "$1" != "" ]]; do
  PARAM=`echo $1 | awk -F= '{print $1}'`
  VALUE=`echo $1 | sed 's/^[^=]*=//g'`
  case ${PARAM} in
    -h | --help)
      print_usage
      exit
      ;;

    --with-tests)
      BUILD_TESTS=ON ;;

    --prefix)
      PREFIX=${VALUE} ;;

    -*)
      echoerr "unknown parameter \"$PARAM\""
      print_usage
      exit 1
      ;;
    esac
    shift
done

if [[ "$PREFIX" = "" ]]
then
  echoerr "parameter \"--prefix\" is required"
  exit 1
fi

print_state "Configure"

mkdir -p build
cd build

# Check if the directory has been changed
if [[ "$?" != "0" ]]
then
  echoerr "failed to change directory"
  exit 1
fi

cmake -DBUILD_TESTS=${BUILD_TESTS} -DCMAKE_INSTALL_PREFIX=${PREFIX} ..

# Check if CMAKE configuration has finished successfully
if [[ "$?" != "0" ]]
then
  echoerr "failed to configure CMAKE"
  exit 1
fi

if [[ "$BUILD_TESTS" = "ON" ]]
then
  print_state "Build"
  make -j4

  # Check if compilation finished successfully
  if [[ "$?" != "0" ]]
  then
    echoerr "failed to compile"
    exit 1
  fi
fi

print_state "Install"

make install

# Check if installation finished successfully
if [[ "$?" != "0" ]]
then
  echoerr "failed to install"
  exit 1
fi

if [[ "$BUILD_TESTS" = "OFF" ]]
then
  print_state "Success"
  exit 0
fi

print_state "Run tests"

#run tests
${PREFIX}/bin/scf_unit_tests

if [[ "$?" != "0" ]]
then
  echoerr "Tests filed"
  exit 1
fi

print_state "Success"
